Following the analysis of the most requested informative areas by seniors, alongside with the related web sources to extract information from, and the analysis of the functional components to be implemented in the platform, the platforms’system’s architecture was designed.

To produce the informative videos, a webcrawler developed by the +TV4E researchers analyses textual content obtained from the defined informative sources (websites and portals previously associated with each ASGIE), in order to gather information to use in these informative videos.The webcrawler is fully automated and its main task is to verify if there is new and relevant information available for the users. Being that the case, the system seeks for certain keywords within this information in order to filter in news pieces that are considered relevant for seniors. The used keywords were defined by the research team and were carefully chosen so that every senior’s interest and needs are included.
Following the previously described proceeding, audio-visual content is automatically generated through a process in which the Automated Video Engine considers parameters such as a text-to-speech narration and visual elements (background colour, icon identifying each informative category, font type, and size, etc.) to include in the videos. 

These videos are sent to the users every half an hour while using the developed iTV platform and according to a set of rules defined by the recommendation system (user’s location and favourite topics). The received informative content is overlaid with the linear television broadcast, appearing a notification on screen asking the senior if he wants to watch the video. If so, the linear television broadcast is paused and resumed after the informative video is closed.
To design the referred architecture, it was first necessary for the development team to conduct a thorough technical analysis of several available tools, preferentially those which were free of charge.  To create an automatic text-to-speech narration of the content three different alternatives where considered: IBM Watson, Ivona and ReadSpeaker. From all of these Ivona, currently bought by Amazon and renamed as Polly, was the chosen one, once it features the Portuguese from Portugal language alternative, its speech quality was very satisfactory and because, when considering the price-quality overall, it was the most economical solution. Whereas, to produce and encode the videos it was used FFmpeg, which as support for Javascript, which is the Automated Video Engine’s main programming language.
## CrawlerToVideo
# Crawler To video
Main Object which connects the entire video production process

**Kind**: global class  

* [CrawlerToVideo](#markdown-header-crawlertovideo)
    * [new CrawlerToVideo(data)](#markdown-header-new-crawlertovideodata)
    * [.crawlStep_2()](#markdown-header-crawlertovideocrawlstep_2)
    * [.crawlStep_3(webResult, source)](#markdown-header-crawlertovideocrawlstep_3webresult-source)
    * [.crawlStep_4(webResult, media, temp, filename, source)](#markdown-header-crawlertovideocrawlstep_4webresult-media-temp-filename-source)

### new CrawlerToVideo(data)

| Param | Type | Description |
| --- | --- | --- |
| data | object | Informative source data |

### crawlerToVideo.crawlStep_2()
## 2. Crawler Step 2
* Crawls the Web For content
* This step checks if the informative video was already created and in the case of 
  CartaSocial makes a request to know if it is ready to be created

**Kind**: instance method of [CrawlerToVideo](#markdown-header-new-crawlertovideodata)  
### crawlerToVideo.crawlStep_3(webResult, source)
## 3. Crawler Step 3

**Kind**: instance method of [CrawlerToVideo](#markdown-header-new-crawlertovideodata)  

| Param | Type | Description |
| --- | --- | --- |
| webResult | object | Results obtained from web Crawling |
| source | object | Informative source data * Creates temporary directories for the video creation process * Fetches media resources for ASGIE (images of that ASGIE) |

### crawlerToVideo.crawlStep_4(webResult, media, temp, filename, source)
## 4. Crawler Step 4

**Kind**: instance method of [CrawlerToVideo](#markdown-header-new-crawlertovideodata)  

| Param | Type | Description |
| --- | --- | --- |
| webResult | object | Results obtained from web Crawling |
| media | object | Media resources for ASGIE (images of that ASGIE) |
| temp | object | Temporary folder created |
| filename | object | Video filename |
| source | object | Informative source data Creates the videos based on the passed parameters `new Dynamicideo({},(duration)=>{});` |

## crawlStep_1(_response)
## 1. Crawler Step 1

**Kind**: global function  

| Param | Type | De## filtersPromise()
### Filters News 
Filters videos according to the requested filters from the database. If a 
news contains a certain word it is created, contains is true.

**Kind**: global function  
## segregate()
### Handle Text - segregate 
Splits text into sentences on every period taking certain prefixes into account. Tools used by Webscrapper.jsx to find certain text patterns and perform certain operations

**Kind**: global function  
## splitter()
### Handle Text - splitter
Splits text based on a length.Tools used by Webscrapper.jsx to find certain text patterns and perform certain operations

**Kind**: global function  
## splitterWithSpaces()
### Handle Text - splitterWithSpaces
Splits text based on a length, includes spaces on the returned array. Tools used by Webscrapper.jsx to find certain text patterns and perform certain operations

**Kind**: global function  
## substring()
### Handle Text - substring
Trims a string from an Index within a length limit .Tools used by Webscrapper.jsx to find certain text patterns and perform certain operations

**Kind**: global function  
## splitSections()
### Handle Text - splitSections
Splits into a number of lines. Tools used by Webscrapper.jsx to find certain text patterns and perform certain operations

**Kind**: global function  
## splitOnPosition()
### Handle Text - splitOnPosition
Splits in certain index .Tools used by Webscrapper.jsx to find certain text patterns and perform certain operations

**Kind**: global function  
## summarize()
`Deprecated` Handle Text - handleURL
Resolves an URL. Tools used by Webscrapper.jsx to find certain text patterns and perform certain operations

**Kind**: global function  
## handleURL()
### Handle Text - handleURL
Resolves an URL. Tools used by Webscrapper.jsx to find certain text patterns and perform certain operations

**Kind**: global function  
## parseURL()
### Handle Text - parseURL
Parses the host from an URL. Tools used by Webscrapper.jsx to find certain text patterns and perform certain operations

**Kind**: global function  
## ffmpegEscape()
### Handle Text - ffmpegEscape
Escapes text with a regex based on ffmpeg rules. Tools used by Webscrapper.jsx to find certain text patterns and perform certain operations

**Kind**: global function  
## generalEscape()
###Handle Text - generalEscape
Escapes text with a regex based on ffmpeg rules. Tools used by Webscrapper.jsx to find certain text patterns and perform certain operations

**Kind**: global function  
scription |
| --- | --- | --- |
| _response | object | List of all the informative sources |

## _checkDirs()
### Check Dirs 
Check the available directories on `configs/Config.jsx > dirs   `

**Kind**: global function  

## infoLog
# Logger
Component to ease the creation of erroLogs, infoLogs, successLogs and requestLogs
Uses Winstons

**Kind**: global constant

## MergeVideos
# MergeVideos
Merges the created slides to a video using ffmeg and adds the process to an Async queue

**Kind**: global class  

## MergeVideos2
# MergeVideos2
Merges the created video with the intro and outro to a video using ffmeg and adds the process to an Async queue

**Kind**: global class    


