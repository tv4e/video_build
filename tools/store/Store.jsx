/**
* # Store
* Storage built using the singleton module
* Stores data globally
*/

class Store {
    constructor() {
        this.token = null;
        this.filters = null;
    }
}

export default (new Store);