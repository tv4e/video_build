'use strict';

const ffmpeg = require('fluent-ffmpeg');
const filters = require('./filters');

import async from 'async';
import {videosConcurrency} from './../../../configs/Config';
import AsyncParallel from './../../middleware/AsyncParallel';

/**
* #Image to Video
* Transforms a slide image to a video and adds the process to an Async queue
*/
class ImageToVideo {

    constructor() {
        // this.q = async.queue(this.run, videosConcurrency.images);// assign a callback
        //
        // this.q.drain = function() {
        //     console.log('items processed');
        // };
        // AsyncParallel.queue(this.run);// assign a callback

        // this.callback = callback;
        // this.image = image;
        // this.tempPath = tempPath;
    }

    run(data, callback){
        let image = data.image;
        let tempPath = data.tempPath;
        let promiseCallback = data.callback;

        let commonOptions = [
            '-pix_fmt yuv420p',
            '-preset ultrafast',
        ];

        ffmpeg(image.path)
            .loop(image.loopTime)
            // .videoFilters(filters.commonFilters().concat(image.hasOwnProperty("videoFilters")==true ? image.videoFilters : []))
            .fps(1)
            .videoCodec('libx264')
            .aspect('16:9')
            .on('end', function () {
                console.log('file has been converted succesfully ');
                callback();
                promiseCallback();
            })
            .on('start', function () {
                console.log('Conversion Started');
            })
            .on('error', function (err,stdout, stderr) {
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
                console.log('an error happened: ' + err.message);
            })
            .outputOptions(commonOptions)
            .save(`${tempPath}/temp${image.index}.mp4`);
    }

    push(image, tempPath, callback){
        // this.q.push({image, tempPath, callback});
        AsyncParallel.push({func:this.run,data:{image, tempPath, callback}});// assign a callback
    }
}

export default (new ImageToVideo);

