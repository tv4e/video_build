'use strict';

const ffmpeg = require('fluent-ffmpeg');
import {dirs} from './../../../configs/Config.jsx';
import {videosConcurrency} from './../../../configs/Config';
import async from 'async';
import AsyncParallel from './../../middleware/AsyncParallel';
import ffProbe from 'node-ffprobe';

/**
* # MergeVideos2
* Merges the created video with the intro and outro to a video using ffmeg and adds the process to an Async queue
*/
class MergeVideos2 {

    constructor() {
        // this.q = async.queue(this.run, videosConcurrency.mergeVideos);// assign a callback
        //
        // this.q.drain = function() {
        //     console.log('items processed');
        // };
    }

    run(data,callback){
        let videosCreated = data.videosCreated;
        let audio = data.audio;
        let music = data.music;
        let filename = data.filename;
        let tempPath = data.tempPath;
        let promiseCallback = data.callback;
        let videos = data.videos;
        let outputVideo = ffmpeg();

        outputVideo
            .input(videos.videoIntro)
            .input(`${tempPath}/${filename}.mp4`)
            .input(videos.videoOutro);


        outputVideo
            .fps(25)
            .aspect('16:9')
            .on('start', function () {
                console.log('Merge Started');
            })
            .on('end', function () {
                console.log('files have been merged succesfully');

                ffProbe(`${dirs.video}/${filename}.mp4`, function (err, probeData) {
                    if (err){
                        console.log('ERROR POLLY ' + err);
                        return null;
                    }
                    callback();
                    promiseCallback(probeData.streams[0].duration);
                });


            })
            .on('error', function (err, stdout, stderr) {
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
                console.log('an error happened: ' + err.message);
            })
            .outputOptions(['-profile baseline','-preset ultrafast'])
            .mergeToFile(`${dirs.video}/${filename}.mp4`);

    }

    push(videosCreated, videos, audio, music,tempPath, filename, callback){
        AsyncParallel.push({func:this.run,data:{videosCreated, videos, audio, music,tempPath, filename, callback}});// assign a callback
        // this.q.push({videosCreated, videos, audio, music,tempPath, filename, callback});
    }
}

export default (new MergeVideos2);

