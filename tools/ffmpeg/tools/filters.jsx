'use strict';
import  HandleText from './../../HandleText';
import {vidRender} from './../../../configs/Config';

/**
* # Filters
* Fiters used by ffmpeg
*/
let foregroundColor = function(color){
    let result = [
        {
            filter: 'drawbox',
            options: '0:0:2000:2000:color='+color+'@0.8:t=max'
        }
    ];

    return result;
};

let commonFilters = function(){
    let result = [
        {
            filter: 'scale',
            options: `${vidRender.width}:${vidRender.height}`
        },
        {
            filter: 'setsar',
            options: '1:1'
        }
    ];
    return result;
};

//Previously used before canvas to deal with text drawing on each image

// let drawText = function(text, position, size, color){
//     let x = position.x;
//     let y = position.y;
//
//     text = HandleText.ffmpegEscape(text);
//
//     let result =  {
//         filter: 'drawtext',
//         options: "fontfile=/usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-L.ttf:\
//                     text='"+text+"':\
//                     expansion=none:\
//                     x="+x+": y='"+y+"': \
//                     fontsize="+size+":\
//                     fontcolor="+color+""
//     };
//     return result;
// };

module.exports = {
    foregroundColor:foregroundColor,
    commonFilters:commonFilters,
    // drawText:drawText
};