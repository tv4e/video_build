'use strict';

const ffmpeg = require('fluent-ffmpeg');
import {videosConcurrency} from './../../../configs/Config';
import async from 'async';
import MergeVideos2 from './MergeVideos2';
import AsyncParallel from './../../middleware/AsyncParallel';

/**
* # MergeVideos
* Merges the created slides to a video using ffmeg and adds the process to an Async queue
*/
class MergeVideos {

    constructor() {
        // this.q = async.queue(this.run, videosConcurrency.mergeImages);// assign a callback
        //
        // this.q.drain = function() {
        //     console.log('items processed');
        // };
    }

    run(data,callback){
        let videosCreated = data.videosCreated;
        let videos = data.videos;
        let audio = data.audio;
        let music = data.music;
        let tempPath = data.tempPath;
        let filename = data.filename;
        let promiseCallback = data.callback;
        let bulletinBoard = data.bulletinBoard;

        let outputVideo = ffmpeg();

        let outputOptions = ['-i ' + audio, '-i '+ music,
            '-filter_complex ['+videosCreated+':0]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=2[a1];['+(videosCreated+1)+':0]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=0.05,apad[a2];[a1][a2]amerge,pan=stereo:c0<c0+c2:c1<c1+c3[out]',
            '-map [out]'
        ];

        if(bulletinBoard){
            outputOptions = ['-i ' + audio, '-i '+ music,
                '-filter_complex ['+videosCreated+':0]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=2,apad[a1];['+(videosCreated+1)+':0]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=0.05,apad[a2];[a1][a2]amerge,pan=stereo:c0<c0+c2:c1<c1+c3[out]',
                '-shortest', '-profile baseline','-preset ultrafast','-map [out]'
            ];
        }

        for (let i = 0; i < videosCreated; i++){
            outputVideo
                .input(tempPath +'/temp'+i+'.mp4');
        }

        outputVideo
            .fps(25)
            .aspect('16:9')
            .on('start', function () {
                console.log('Merge Images');
            })
            .on('end', function () {
                callback();
                MergeVideos2.push(videosCreated, videos, audio, music,tempPath, filename, promiseCallback);
            })
            .on('error', function (err, stdout, stderr) {
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
                console.log('an error happened: ' + err.message);
            })
            .outputOptions(outputOptions)
            .mergeToFile(`${tempPath}/${filename}.mp4`);

    }

    push(videosCreated, videos, audio, music,tempPath, filename, bulletinBoard, callback){
        AsyncParallel.push({func:this.run, data:{videosCreated, videos, audio, music,tempPath, filename, bulletinBoard, callback}});// assign a callback
        // this.q.push({videosCreated, videos, audio, music,tempPath, filename, callback});
    }
}

export default (new MergeVideos);

