'use strict';
//Request is designed to be the simplest way possible to make http calls.
// It supports HTTPS and follows redirects by default.
import AWS from 'aws-sdk';
import fs from 'fs-extra';
import HandleText from './../HandleText';
import Speech from 'ssml-builder';
import webAudioApi from 'web-audio-api';
import _ from 'lodash';

export default class Polly{

    constructor(text, filePath, callback) {
        this.text = text;
        this.filePath = filePath;
        this.callback = callback;
        this.bufferArray = null;
    }

    synth(){
        let filePath = this.filePath;
        let text = this.text;
        let callback = this.callback;
        let bufferArray = this.bufferArray;
        let junks = 0;
        let splitText = [];

        text.forEach((v, k)=>{
            v.forEach((v2)=>{
                if(v2.ssml){
                    splitText.push({ssml:v2.ssml,index:k, duration: 0});
                    junks++;
                } else if(v2.text){
                    HandleText.splitterWithSpaces(v2.text, 400).forEach((string)=>{
                        splitText.push({text:string, index:k, duration: 0});
                        junks++;
                    });
                }
            });
        });


        bufferArray = [];

        // Create an Polly client
        const Polly = new AWS.Polly({
            signatureVersion: 'v4',
            region: 'eu-west-1'
        });

        splitText.forEach((textJunk, i)=>{
            let speechOutput = "";

            if(textJunk.text){
                let speech = new Speech().say(textJunk.text);
                speechOutput = speech.ssml(true);
            } else if(textJunk.ssml){
                speechOutput = textJunk.ssml;
            }

            let params = {
                'Text': `<speak><prosody rate="90%">${speechOutput}</prosody></speak>`,
                'OutputFormat': 'mp3',
                'VoiceId': 'Ines',
                'TextType': 'ssml'
            };

            Polly.synthesizeSpeech(params,(err, data) => {
                if (err) {
                    console.log(textJunk);
                    console.log(err.code)
                } else if (data) {
                    if (data.AudioStream instanceof Buffer) {
                        let AudioContext = new webAudioApi.AudioContext(Buffer);
                        AudioContext.decodeAudioData(data.AudioStream, function(abuffer) {
                            textJunk.duration = abuffer.duration;
                            bufferArray[i] = data.AudioStream;
                            junks--;
                            if(junks==0){
                                writeFile();
                            }
                        });

                    }
                }
            })
        });


         function writeFile(){
             let newBuffer = Buffer.concat(bufferArray);
             let duration = [];

             fs.writeFile(`${filePath}.mp3`, newBuffer, function(err) {
                if (err) {
                    return console.log(err)
                }

                 splitText.forEach((v)=>{
                     if(duration[v.index] == undefined){
                         duration.splice(v.index, 0, v.duration);
                     } else {
                         duration[v.index] = duration[v.index] + v.duration;
                     }
                 });

                callback(duration);
            })
        }
    }
}
