'use strict';

import async from 'async';
import {videosConcurrency} from './../../configs/Config';

/**
* # AsyncParallel 
* Allows a certain number of objects from a class to be created at the same time
*/
class AsyncParallel {
    constructor() {
        this.q = async.queue((data,callback)=>{
            data.func(data.data, callback);
        }, videosConcurrency);// assign a callback

        this.q.drain = function() {
            console.log('items processed');
        };
    }

    push(data){
        this.q.push(data);
    }

}

export default (new AsyncParallel);