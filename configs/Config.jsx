'use strict';

  /**
  * # Configs
  * Configuration used by the software
  */
export let dirs = {
        root:'/assets/+tv4e',
        images:'/assets/+tv4e/images',
        video: '/assets/+tv4e/videos/informative',
        audio:'/assets/+tv4e/audios'
};

  /**
  * `videoConcurrency -> int`
  * Number of video that can be created at a time
  */
export let videosConcurrency = 3;

export let vidRender = {
        titleSize:'60px Tiresias',
        titleBigSize:'70px Tiresias',
        textSize:'50px Tiresias',
        titleLines: 2,
        descLines: 5,
        cartaSocialdescLines: 5,
        cartaSocialLoop: 10,
        width:1280,
        height:720,
        ySpacing:75,
        // @TODO set here distance to title firstYdesc:'SET HERE'
};