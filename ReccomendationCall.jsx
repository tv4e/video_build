const axios = require('axios');

let maxX = 1;
let x = 0;
let twentyMinutes = 6000*20;

_axios();
let intervalID = setInterval(function () {
  _axios();
}, twentyMinutes);

function _axios () {
  if (++x === maxX) {
    console.log('Force terminate majordomo');
    clearInterval(intervalID);
    process.exit()
  }

  axios({
  method:'get',
  url:'http://79.137.39.168:8080/majordomo/refresh_recommendations',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Accept' : 'application/x-www-form-urlencoded'
  }
})
  .then((response) => {
    if (response.status != 200) {
      console.log('error');
      clearInterval(intervalID);
      return 'error';
    }
    console.log('OK majordomo');
    clearInterval(intervalID);
  })
  .catch((error) => {
    clearInterval(intervalID);
    console.log(error);
    return error;
  });
}